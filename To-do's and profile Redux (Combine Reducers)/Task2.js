import { combineReducers, createStore } from "redux";
import { useDispatch, useSelector } from "react-redux";

/**
*=============================================
=            TO-DO's            =
=============================================*/

const action = (data) => {
    return {
        type: "CHANGE",
        payload: data
    }
}

// const initialState = { value: "", tasksList: [], show: false }
const initialState = { value: "", tasksList: [] }

const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        // case "SHOW":
        //     return {
        //         ...state,
        //         show: !state.show
        //     }
        case "ADD_TASK":
            // alert("Task '" + action.payload + "' Added");
            return {
                ...state,
                tasksList: [...state.tasksList, action.payload]
            }
        case "CHANGE":
            return {
                ...state,
                value: action.payload
            }
        case "DELETE":
            let list = state.tasksList.filter((item, index) => index !== action.payload)
            let data = {
                ...state,
                tasksList: list
            }
            console.log(state, list)
            return data
        default: return state
    }
}

export function AddToDoComponent() {
    const dispatch = useDispatch();
    const value = useSelector(state => state.todo.value);
    return <div>
        <h2>To-do</h2>
        <input
            placeholder="Enter Task"
            onChange={(event) => dispatch(action(event.target.value))}
        />
        <button
            onClick={() =>
                dispatch({ type: "ADD_TASK", payload: value })}>
            Add Task
        </button>
    </div>
}


export function DisplayToDoComponent() {
    const dispatch = useDispatch();
    const tasksList = useSelector(state => {
        console
            .log(state.todo.tasksList); return state.todo.tasksList
    });
    // const show = useSelector(state => state.todo.show);
    // if (show) {
    let display = tasksList.map((item, index) => {
        return <div key={index} id={index} >
            <li > {item}
                <code onClick={() => dispatch({ type: "DELETE", payload: index })}> delete</code>
            </li>
        </div >
    },
    )
    // }
    return <div>
        <p>Number of Tasks: {tasksList.length}</p>
        {/* <button onClick={() => dispatch({ type: "SHOW" })}>Show Tasks</button> */}
        <ul>
            {display}
        </ul>
    </div >
}


/*=====  End of TO-DO's  ======*
*
*/





/*=============================================
=            PROFILE            =
=============================================*/

const actionP = (data) => {
    return {
        type: "UPDATE",
        payload: data
    }
}

const initialStateP = {
    name: "",
    email: "",
    info: []
}

const profileReducer = (state = initialStateP, action) => {
    switch (action.type) {
        case "CHANGE_NAME":
            return {
                ...state,
                name: action.payload
            }
        case "CHANGE_EMAIL":
            return {
                ...state,
                email: action.payload
            }
        case "UPDATE":
            return {
                ...state,
                info: [state.name, state.email]
            }
        default: return state
    }
}

export function EditProfileComponent() {
    const dispatch = useDispatch()
    return <div>
        <h2>Profile</h2>
        <input
            type="text"
            placeholder="Enter Name"
            onChange={(event) => dispatch({ type: "CHANGE_NAME", payload: event.target.value })}
        />
        <input
            type="email"
            placeholder="Enter Email"
            onChange={(event) => dispatch({ type: "CHANGE_EMAIL", payload: event.target.value })} />
        <button
            onClick={() => dispatch({ type: "UPDATE" })}>Save Profile</button>
    </div>
}

export function ViewProfileComponent() {
    const info = useSelector(state => { console.log(state.profile.info); return state.profile.info });
    return <div>
        <p><b>Name:</b>{info[0]}</p>
        <p><b>Email:</b>{info[1]}</p>
    </div>
}

/*=====  End of PROFILE  ======*/

const reducer = combineReducers({ todo: todoReducer, profile: profileReducer })

export const store = createStore(reducer);
